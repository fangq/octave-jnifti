Commands to build the package, please copy and paste to a terminal

```
VER=0.6
PKG=jnifti
wget https://github.com/fangq/${PKG}/archive/v${VER}.tar.gz
tar zxvf v${VER}.tar.gz
# remove upstream binary files
cd ${PKG}-${VER}
mkdir inst
mkdir src
mv lib/matlab/*.m inst
mv lib/octave/*.m inst
mv lib/matlab/LICENSE_GPLv3.txt COPYING
sed -i -e 's/^%!.*$//g' inst/niftiread.m inst/niftiwrite.m inst/niftiinfo.m

cat > DESCRIPTION << EOF
Name: jnifti
Version: ${VER}
Date: 2020-15-06
Title: fast NIfTI-1/2 reader and NIfTI-to-JNIfTI converter for MATLAB/Octave
Author: Qianqian Fang <fangqq@gmail.com>
Maintainer: Qianqian Fang <fangqq@gmail.com>
Description: JNIfTI Toolbox is a fully functional NIfTI-1/2 reader/writer that supports both
 MATLAB and GNU Octave, and is capable of reading/writing both non-compressed
 and compressed NIfTI files (.nii, .nii.gz) as well as two-part Analyze7.5/NIfTI
 files (.hdr/.img and .hdr.gz/.img.gz). 
 More importantly, this is a toolbox that converts NIfTI data to its JSON-based
 replacement, JNIfTI (.jnii for text-based and .bnii for binary-based), defined
 by the JNIfTI specification (http://github.com/fangq/jnifti). JNIfTI is a
 much more flexible, human-readable and extensible file format compared to the
 more rigid and opaque NIfTI format, making the data much easier to manipulate
 and share.
URL: https://github.com/fangq/jnifti
Depends: jsonlab, zmat
EOF

cat > INDEX << EOF
jnifti >> JNIfTI
JNIfTI
 jnifticreate
 loadjnifti
 loadnifti
 memmapstream
 nifticreate
 niftiread
 niftiinfo
 niftiwrite
 nii2jnii
 jnii2nii
 niicodemap
 niiformat
 savebnii
 savejnifti
 savejnii
 savenifti
EOF

cd ..

# recreate the orig package
tar zcvf octave-${PKG}_${VER}.orig.tar.gz ${PKG}-${VER}
cd ${PKG}-${VER}

#download the debian packaging files
git clone https://salsa.debian.org/fangq/octave-${PKG}.git debian

#build deb package
debuild -us -uc
```
